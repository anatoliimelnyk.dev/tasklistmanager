﻿using TaskListManager.Domain.Errors;
using TaskListManager.Domain.Primitives;
using TaskListManager.Domain.Shared;

namespace TaskListManager.Domain.ValueObjects;

public sealed class TaskListName : ValueObject
{
    public const int MaxLength = 255;

    private TaskListName(string value)
    {
        Value = value;
    }

    public string Value { get; }

    public static Result<TaskListName> Create(string taskListName)
    {
        if (string.IsNullOrWhiteSpace(taskListName))
        {
            return Result.Failure<TaskListName>(DomainErrors.TaskList.Empty);
        }

        if (taskListName.Length > MaxLength)
        {
            return Result.Failure<TaskListName>(DomainErrors.TaskList.TooLong);
        }

        return new TaskListName(taskListName);
    }

    public override IEnumerable<object> GetAtomicValues()
    {
        yield return Value;
    }
}