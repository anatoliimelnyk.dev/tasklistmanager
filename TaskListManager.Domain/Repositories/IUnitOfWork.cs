﻿namespace TaskListManager.Domain.Repositories;

public interface IUnitOfWork
{
    public Task SaveChangesAsync(CancellationToken cancellationToken = default);

    public ITaskListRepository TaskListRepository { get; }
    public IUserRepository UserRepository { get; }
}
