﻿using TaskListManager.Domain.Entities;

namespace TaskListManager.Domain.Repositories;

public interface ITaskListRepository
{
    Task<TaskList?> GetByIdAsync(Guid id, Guid userId, CancellationToken cancellationToken = default);

    Task<TaskList?> GetByIdForRemoveAsync(Guid id, Guid userId, CancellationToken cancellationToken = default);

    Task<IEnumerable<TaskList>> GetAllAsync(Guid userId, int skip, int take, CancellationToken cancellationToken = default);

    Task<IEnumerable<User>> GetRelatedUsersByTaskListIdAsync(Guid id, Guid userId, CancellationToken cancellationToken = default);

    void Add(TaskList taskList);

    void Update(TaskList taskList);

    void Remove(TaskList taskList);
}
