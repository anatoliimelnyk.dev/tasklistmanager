﻿using TaskListManager.Domain.Entities;
using TaskListManager.Domain.ValueObjects;

namespace TaskListManager.Domain.Repositories;

public interface IUserRepository
{
    Task<User?> GetByIdAsync(Guid id, CancellationToken cancellationToken = default);

    Task<bool> IsEmailUniqueAsync(Email email, CancellationToken cancellationToken = default);

    void Add(User user);

    void Update(User user);
}
