﻿using TaskListManager.Domain.Shared;

namespace TaskListManager.Domain.Errors;

public static class DomainErrors
{
    public static class User
    {
        public static readonly Error EmailAlreadyInUse = new(
            "User.EmailAlreadyInUse",
            "The specified email is already in use");

        public static readonly Func<Guid, Error> NotFound = id => new Error(
            "User.NotFound",
            $"The user with the identifier {id} was not found.");

        public static readonly Func<Guid, Guid, Error> NotAddedToTaskList = (userId, taskId) => new Error(
            "User.NotFound",
            $"The user with the identifier {userId} was not conected with TaskList {taskId}.");
    }

    public static class TaskList
    {
        public static readonly Func<Guid, Error> NotFound = id => new Error(
            "TaskList.NotFound",
            $"TaskList with the identifier {id} was not found.");

        public static readonly Error Empty = new(
            "TaskListName.Empty",
            "TaskList name is empty");

        public static readonly Error TooLong = new(
            "TaskList.TooLong",
            "TaskList name is too long");
    }

    public static class Email
    {
        public static readonly Error Empty = new(
            "Email.Empty",
            "Email is empty");

        public static readonly Error InvalidFormat = new(
            "Email.InvalidFormat",
            "Email format is invalid");
    }

    public static class FirstName
    {
        public static readonly Error Empty = new(
            "FirstName.Empty",
            "First name is empty");

        public static readonly Error TooLong = new(
            "LastName.TooLong",
            "FirstName name is too long");
    }

    public static class LastName
    {
        public static readonly Error Empty = new(
            "LastName.Empty",
            "Last name is empty");

        public static readonly Error TooLong = new(
            "LastName.TooLong",
            "Last name is too long");
    }
}
