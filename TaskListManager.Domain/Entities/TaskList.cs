﻿using TaskListManager.Domain.Primitives;
using TaskListManager.Domain.ValueObjects;

namespace TaskListManager.Domain.Entities;

public sealed class TaskList : Entity, IAuditableEntity
{
    public TaskList(Guid id, TaskListName name, User owner)
        : base(id)
    {
        Name = name;
        Owner = owner;
    }

    /// <summary>
    /// EF constructor
    /// </summary>
    public TaskList(Guid id)
        : base(id) { }

    public TaskListName Name { get; set; }

    public ICollection<TaskListUser> TaskListUsers { get; set; } = new List<TaskListUser>();

    public User Owner { get; set; }

    public DateTime CreatedOnUtc { get; set; }

    public DateTime? ModifiedOnUtc { get; set; }

    public static TaskList Create(
        Guid id,
        TaskListName name,
        User owner)
    {
        var taskList = new TaskList(
            id,
            name,
            owner);

        return taskList;
    }

    public void ChangeTaskList(TaskListName name)
    {
        Name = name;
    }

    public void AddUser(User user)
    {
        var taskListUser = new TaskListUser(Guid.NewGuid())
        {
            User = user,
            TaskList = this,
            UserId = user.Id,
            TaskListId = Id
        };

        TaskListUsers.Add(taskListUser);
    }

    public bool RemoveUser(User user)
    {
        var taskListUser = TaskListUsers.FirstOrDefault(t => t.User == user);
        if(taskListUser == null)
        {
            return false;
        }

        TaskListUsers.Remove(taskListUser);

        return true;
    }

}
