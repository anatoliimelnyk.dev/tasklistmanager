﻿using TaskListManager.Domain.Primitives;

namespace TaskListManager.Domain.Entities;

public sealed class TaskListUser : Entity
{
    public TaskListUser(Guid id) 
        : base(id) { }

    public Guid TaskListId { get; set; }

    public TaskList TaskList { get; set; }

    public Guid UserId { get; set; }

    public User User { get; set; }
}
