﻿using TaskListManager.Domain.Primitives;
using TaskListManager.Domain.ValueObjects;

namespace TaskListManager.Domain.Entities;

public sealed class User : Entity, IAuditableEntity
{
    private User(Guid id, Email email, FirstName firstName, LastName lastName)
        : base(id)
    {
        Email = email;
        FirstName = firstName;
        LastName = lastName;
    }

    public Email Email { get; private set; }

    public FirstName FirstName { get; set; }

    public LastName LastName { get; set; }

    public ICollection<TaskListUser> TaskListUsers { get; set; }

    public ICollection<TaskList> OwnedTaskList { get; set; }

    public DateTime CreatedOnUtc { get; set; }

    public DateTime? ModifiedOnUtc { get; set; }

    public static User Create(
        Guid id,
        Email email,
        FirstName firstName,
        LastName lastName)
    {
        var user = new User(
            id,
            email,
            firstName,
            lastName);

        return user;
    }

    public void ChangeName(FirstName firstName, LastName lastName)
    {
        FirstName = firstName;
        LastName = lastName;
    }
}
