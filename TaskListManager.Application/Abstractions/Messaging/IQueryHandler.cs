﻿using MediatR;
using TaskListManager.Domain.Shared;

namespace TaskListManager.Application.Abstractions.Messaging;

public interface IQueryHandler<TQuery, TResponse>
    : IRequestHandler<TQuery, Result<TResponse>>
    where TQuery : IQuery<TResponse>
{
}