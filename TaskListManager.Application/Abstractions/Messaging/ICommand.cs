﻿using MediatR;
using TaskListManager.Domain.Shared;

namespace TaskListManager.Application.Abstractions.Messaging;

public interface ICommand : IRequest<Result>
{
}

public interface ICommand<TResponse> : IRequest<Result<TResponse>>
{
}
