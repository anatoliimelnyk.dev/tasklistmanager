﻿using MediatR;
using TaskListManager.Domain.Shared;

namespace TaskListManager.Application.Abstractions.Messaging;

public interface IQuery<TResponse> : IRequest<Result<TResponse>>
{
}