﻿using TaskListManager.Application.Abstractions.Messaging;

namespace TaskListManager.Application.User.Queries.GetUserById;

public sealed record GetUserByIdQuery(Guid UserId) : IQuery<UserResponse>;