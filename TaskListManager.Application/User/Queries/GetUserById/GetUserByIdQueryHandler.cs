﻿using TaskListManager.Application.Abstractions.Messaging;
using TaskListManager.Domain.Repositories;
using TaskListManager.Domain.Shared;

namespace TaskListManager.Application.User.Queries.GetUserById;

internal sealed class GetUserByIdQueryHandler
    : IQueryHandler<GetUserByIdQuery, UserResponse>
{
    private readonly IUnitOfWork _unitOfWork;

    public GetUserByIdQueryHandler(IUnitOfWork unitOfWork)
    {
        _unitOfWork = unitOfWork;
    }

    public async Task<Result<UserResponse>> Handle(
        GetUserByIdQuery request,
        CancellationToken cancellationToken)
    {
        var user = await _unitOfWork.UserRepository.GetByIdAsync(
            request.UserId,
            cancellationToken);

        if (user is null)
        {
            return Result.Failure<UserResponse>(new Error(
                "User.NotFound",
                $"The user with Id {request.UserId} was not found"));
        }

        var response = new UserResponse(user.Id, user.Email.Value);

        return response;
    }
}