﻿namespace TaskListManager.Application.User.Queries;

public sealed record UserResponse(Guid Id, string Email);