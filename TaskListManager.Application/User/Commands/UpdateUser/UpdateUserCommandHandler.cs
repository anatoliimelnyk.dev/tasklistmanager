﻿using TaskListManager.Application.Abstractions.Messaging;
using TaskListManager.Domain.Errors;
using TaskListManager.Domain.Repositories;
using TaskListManager.Domain.Shared;
using TaskListManager.Domain.ValueObjects;

namespace TaskListManager.Application.User.Commands.UpdateUser;

internal sealed class UpdateUserCommandHandler : ICommandHandler<UpdateUserCommand>
{
    private readonly IUnitOfWork _unitOfWork;

    public UpdateUserCommandHandler(IUnitOfWork unitOfWork)
    {
        _unitOfWork = unitOfWork;
    }

    public async Task<Result> Handle(
        UpdateUserCommand request,
        CancellationToken cancellationToken)
    {
        Domain.Entities.User? user = await _unitOfWork.UserRepository.GetByIdAsync(
            request.UserId,
            cancellationToken);

        if (user is null)
        {
            return Result.Failure(
                DomainErrors.User.NotFound(request.UserId));
        }

        Result<FirstName> firstNameResult = FirstName.Create(request.FirstName);
        Result<LastName> lastNameResult = LastName.Create(request.LastName);

        user.ChangeName(
            firstNameResult.Value,
            lastNameResult.Value);

        _unitOfWork.UserRepository.Update(user);

        await _unitOfWork.SaveChangesAsync(cancellationToken);

        return Result.Success();
    }
}
