﻿using TaskListManager.Application.Abstractions.Messaging;

namespace TaskListManager.Application.User.Commands.UpdateUser;

public sealed record UpdateUserCommand(Guid UserId, string FirstName, string LastName) : ICommand;
