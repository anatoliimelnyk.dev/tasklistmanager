﻿using TaskListManager.Application.Abstractions.Messaging;

namespace TaskListManager.Application.User.Commands.CreateUser;

public sealed record CreateUserCommand(
    string Email,
    string FirstName,
    string LastName) : ICommand<Guid>;
