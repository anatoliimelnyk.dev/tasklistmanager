﻿using TaskListManager.Application.Abstractions.Messaging;
using TaskListManager.Application.User.Queries;
using TaskListManager.Domain.Entities;
using TaskListManager.Domain.Repositories;
using TaskListManager.Domain.Shared;

namespace TaskListManager.Application.TaskLists.Queries.GetTaskListById;

internal sealed class GetTaskListByIdQueryHandler
    : IQueryHandler<GetTaskListByIdQuery, TaskListResponse>
{
    private readonly IUnitOfWork _unitOfWork;

    public GetTaskListByIdQueryHandler(IUnitOfWork unitOfWork)
    {
        _unitOfWork = unitOfWork;
    }

    public async Task<Result<TaskListResponse>> Handle(
        GetTaskListByIdQuery request,
        CancellationToken cancellationToken)
    {
        TaskList? taskList = await _unitOfWork.TaskListRepository.GetByIdAsync(
            request.TaskListId,
            request.UserId,
            cancellationToken);

        if (taskList is null)
        {
            return Result.Failure<TaskListResponse>(new Error("Task List Not Found", $"Task List {request.TaskListId} was not found."));
        }

        var response = new TaskListResponse(
            taskList.Id,
            taskList.Owner.Id,
            taskList.Name.Value,
            (from user in taskList.TaskListUsers.Select(u => u.User)
             let userResponse = new UserResponse(
                 user.Id,
                 user.Email.Value)
             select userResponse).ToList());

        return response;
    }
}
