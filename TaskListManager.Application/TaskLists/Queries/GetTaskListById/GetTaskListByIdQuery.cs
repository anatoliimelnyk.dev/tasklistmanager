﻿using TaskListManager.Application.Abstractions.Messaging;

namespace TaskListManager.Application.TaskLists.Queries.GetTaskListById;

public sealed record GetTaskListByIdQuery(
    Guid TaskListId, 
    Guid UserId) 
    : IQuery<TaskListResponse>;
