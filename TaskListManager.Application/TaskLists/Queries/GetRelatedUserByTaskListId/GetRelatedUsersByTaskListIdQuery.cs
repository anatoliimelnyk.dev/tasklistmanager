﻿using TaskListManager.Application.Abstractions.Messaging;
using TaskListManager.Application.User.Queries;

namespace TaskListManager.Application.TaskLists.Queries.GerRelatedUsersByTaskId;

public sealed record GetRelatedUsersByTaskListIdQuery(
    Guid TaskListId,
    Guid UserId)
    : IQuery<IEnumerable<UserResponse>>;
