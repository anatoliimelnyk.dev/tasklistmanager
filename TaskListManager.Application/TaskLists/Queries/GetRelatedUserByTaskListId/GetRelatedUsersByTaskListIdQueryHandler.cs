﻿using TaskListManager.Application.Abstractions.Messaging;
using TaskListManager.Application.User.Queries;
using TaskListManager.Domain.Repositories;
using TaskListManager.Domain.Shared;

namespace TaskListManager.Application.TaskLists.Queries.GerRelatedUsersByTaskId;

internal sealed class GetRelatedUsersByTaskListIdQueryHandler
    : IQueryHandler<GetRelatedUsersByTaskListIdQuery, IEnumerable<UserResponse>>
{
    private readonly IUnitOfWork _unitOfWork;

    public GetRelatedUsersByTaskListIdQueryHandler(IUnitOfWork unitOfWork)
    {
        _unitOfWork = unitOfWork;
    }

    public async Task<Result<IEnumerable<UserResponse>>> Handle(GetRelatedUsersByTaskListIdQuery request, CancellationToken cancellationToken)
    {
        var users = await _unitOfWork.TaskListRepository.GetRelatedUsersByTaskListIdAsync(request.TaskListId, request.UserId);

        return (from user in users
                let response = new UserResponse(
                user.Id,
                user.Email.Value)
                select response).ToList();
    }
}
