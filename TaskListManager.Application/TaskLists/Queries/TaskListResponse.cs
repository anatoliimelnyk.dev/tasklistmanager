﻿using TaskListManager.Application.User.Queries;
using TaskListManager.Domain.Entities;

namespace TaskListManager.Application.TaskLists.Queries;

public sealed record TaskListResponse(
    Guid Id,
    Guid OwnerId, 
    string Name,
    ICollection<UserResponse> RelatedUserId)
{
}
