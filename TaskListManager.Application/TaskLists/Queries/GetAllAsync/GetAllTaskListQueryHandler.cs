﻿using TaskListManager.Application.Abstractions.Messaging;
using TaskListManager.Application.User.Queries;
using TaskListManager.Domain.Repositories;
using TaskListManager.Domain.Shared;

namespace TaskListManager.Application.TaskLists.Queries.GetAllAsync;

internal sealed class GetAllTaskListQueryHandler
    : IQueryHandler<GetAllTaskListQuery, IEnumerable<TaskListResponse>>
{
    private readonly IUnitOfWork _unitOfWork;

    public GetAllTaskListQueryHandler(IUnitOfWork unitOfWork)
    {
        _unitOfWork = unitOfWork;
    }

    public async Task<Result<IEnumerable<TaskListResponse>>> Handle(
        GetAllTaskListQuery request,
        CancellationToken cancellationToken)
    {
        var taskLists = await _unitOfWork.TaskListRepository.GetAllAsync(
            request.UserId,
            request.skip,
            request.take,
            cancellationToken);

        return (from taskList in taskLists
                let response = new TaskListResponse(
                taskList.Id,
                taskList.Owner.Id,
                taskList.Name.Value,
                (from user in taskList.TaskListUsers.Select(u => u.User)
                 let userResponse = new UserResponse(
                     user.Id, 
                     user.Email.Value) 
                 select userResponse).ToList())
                select response).ToList();
    }
}
