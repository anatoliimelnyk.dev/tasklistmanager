﻿using TaskListManager.Application.Abstractions.Messaging;

namespace TaskListManager.Application.TaskLists.Queries.GetAllAsync;

public sealed record GetAllTaskListQuery(
    Guid UserId,
    int skip, 
    int take) 
    : IQuery<IEnumerable<TaskListResponse>>;
