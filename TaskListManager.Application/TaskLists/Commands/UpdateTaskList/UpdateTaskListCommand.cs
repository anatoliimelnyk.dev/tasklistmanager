﻿using MediatR;
using TaskListManager.Application.Abstractions.Messaging;

namespace TaskListManager.Application.TaskLists.Commands.UpdateTaskList;

public sealed record UpdateTaskListCommand(
    Guid TaskListId, 
    Guid UserId,
    string Name)
    : ICommand;
