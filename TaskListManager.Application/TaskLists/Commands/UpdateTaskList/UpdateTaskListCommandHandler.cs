﻿using TaskListManager.Application.Abstractions.Messaging;
using TaskListManager.Domain.Errors;
using TaskListManager.Domain.Repositories;
using TaskListManager.Domain.Shared;
using TaskListManager.Domain.ValueObjects;

namespace TaskListManager.Application.TaskLists.Commands.UpdateTaskList;

internal sealed class UpdateTaskListCommandHandler : ICommandHandler<UpdateTaskListCommand>
{
    private readonly IUnitOfWork _unitOfWork;

    public UpdateTaskListCommandHandler(IUnitOfWork unitOfWork)
    {
        _unitOfWork = unitOfWork;
    }

    public async Task<Result> Handle(UpdateTaskListCommand request, CancellationToken cancellationToken)
    {
        Result<TaskListName> taskListNameResult = TaskListName.Create(request.Name);

        var taskList = await _unitOfWork.TaskListRepository.GetByIdAsync(
            request.TaskListId, 
            request.UserId, 
            cancellationToken);

        if(taskList is null)
        {
            return Result.Failure(
                DomainErrors.TaskList.NotFound(request.TaskListId));
        }

        taskList.ChangeTaskList(taskListNameResult.Value);

        _unitOfWork.TaskListRepository.Update(taskList);

        await _unitOfWork.SaveChangesAsync(cancellationToken);

        return Result.Success();
    }
}
