﻿using TaskListManager.Application.Abstractions.Messaging;
using TaskListManager.Application.TaskLists.Commands.RemoveTaskList;
using TaskListManager.Domain.Errors;
using TaskListManager.Domain.Repositories;
using TaskListManager.Domain.Shared;

namespace TaskListManager.Application.TaskLists.Commands.CreateTaskList;

internal sealed class RemoveTaskListCommandHendler : ICommandHandler<RemoveTaskListCommand>
{
    private readonly IUnitOfWork _unitOfWork;

    public RemoveTaskListCommandHendler(IUnitOfWork unitOfWork)
    {
        _unitOfWork = unitOfWork;
    }

    public async Task<Result> Handle(RemoveTaskListCommand request, CancellationToken cancellationToken)
    {
        var taskList = await _unitOfWork.TaskListRepository.GetByIdForRemoveAsync(
            request.TaskListId, 
            request.UserId, 
            cancellationToken);

        if (taskList is null)
        {
            return Result.Failure(
                DomainErrors.TaskList.NotFound(request.TaskListId));
        }

        _unitOfWork.TaskListRepository.Remove(taskList);

        await _unitOfWork.SaveChangesAsync(cancellationToken);

        return Result.Success();
    }
}
