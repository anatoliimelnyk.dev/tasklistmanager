﻿using MediatR;
using TaskListManager.Application.Abstractions.Messaging;

namespace TaskListManager.Application.TaskLists.Commands.RemoveTaskList;

public sealed record RemoveTaskListCommand(
    Guid TaskListId,
    Guid UserId)
    : ICommand;
