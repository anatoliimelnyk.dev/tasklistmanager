﻿using TaskListManager.Application.Abstractions.Messaging;
using TaskListManager.Domain.Errors;
using TaskListManager.Domain.Repositories;
using TaskListManager.Domain.Shared;

namespace TaskListManager.Application.TaskLists.Commands.AddUserToTaskList;

internal sealed class AddUserToTaskListCommandHandler : ICommandHandler<AddUserToTaskListCommand>
{
    private readonly IUnitOfWork _unitOfWork;

    public AddUserToTaskListCommandHandler(IUnitOfWork unitOfWork)
    {
        _unitOfWork = unitOfWork;
    }

    public async Task<Result> Handle(AddUserToTaskListCommand request, CancellationToken cancellationToken)
    {
        var taskList = await _unitOfWork.TaskListRepository.GetByIdAsync(
            request.TaskListId, 
            request.OwnerId, 
            cancellationToken);

        if(taskList is null)
        {
            return Result.Failure(
                DomainErrors.TaskList.NotFound(request.TaskListId));
        }

        var user = await _unitOfWork.UserRepository.GetByIdAsync(
            request.UserId, 
            cancellationToken);

        if(user is null)
        {
            return Result.Failure(
                DomainErrors.User.NotFound(request.UserId));
        }

        taskList.AddUser(user);

        await _unitOfWork.SaveChangesAsync();

        return Result.Success();
    }
}
