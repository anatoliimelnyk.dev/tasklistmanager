﻿using TaskListManager.Application.Abstractions.Messaging;

namespace TaskListManager.Application.TaskLists.Commands.AddUserToTaskList;

public sealed record AddUserToTaskListCommand(
    Guid TaskListId,
    Guid OwnerId,
    Guid UserId) 
    : ICommand;
