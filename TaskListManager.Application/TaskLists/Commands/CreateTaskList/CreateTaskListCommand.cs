﻿using MediatR;
using TaskListManager.Application.Abstractions.Messaging;

namespace TaskListManager.Application.TaskLists.Commands.CreateTaskList;

public sealed record CreateTaskListCommand(
    Guid UserId, 
    string Name)
    : ICommand<Guid>;
