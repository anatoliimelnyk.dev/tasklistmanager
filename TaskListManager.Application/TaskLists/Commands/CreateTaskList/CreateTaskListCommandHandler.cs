﻿using TaskListManager.Application.Abstractions.Messaging;
using TaskListManager.Domain.Entities;
using TaskListManager.Domain.Repositories;
using TaskListManager.Domain.Shared;
using TaskListManager.Domain.ValueObjects;

namespace TaskListManager.Application.TaskLists.Commands.CreateTaskList;

internal sealed class CreateTaskListCommandHandler : ICommandHandler<CreateTaskListCommand, Guid>
{
    private readonly IUnitOfWork _unitOfWork;

    public CreateTaskListCommandHandler(IUnitOfWork unitOfWork)
    {
        _unitOfWork = unitOfWork;
    }

    public async Task<Result<Guid>> Handle(CreateTaskListCommand request, CancellationToken cancellationToken)
    {
        Result<TaskListName> taskListNameResult = TaskListName.Create(request.Name);

        var user = await _unitOfWork.UserRepository.GetByIdAsync(
            request.UserId,
            cancellationToken);

        if (user is null)
        {
            return Guid.Empty;
        }

        var taskList = TaskList.Create(Guid.NewGuid(), taskListNameResult.Value, user);

        _unitOfWork.TaskListRepository.Add(taskList);

        await _unitOfWork.SaveChangesAsync(cancellationToken);

        return taskList.Id;
    }
}
