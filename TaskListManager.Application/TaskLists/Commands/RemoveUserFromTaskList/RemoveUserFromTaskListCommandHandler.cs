﻿using TaskListManager.Application.Abstractions.Messaging;
using TaskListManager.Domain.Errors;
using TaskListManager.Domain.Repositories;
using TaskListManager.Domain.Shared;

namespace TaskListManager.Application.TaskLists.Commands.RemoveUserFromTaskList;

public sealed class RemoveUserFromTaskListCommandHandler : ICommandHandler<RemoveUserFromTaskListCommand>
{
    private readonly IUnitOfWork _unitOfWork;

    public RemoveUserFromTaskListCommandHandler(IUnitOfWork unitOfWork)
    {
        _unitOfWork = unitOfWork;
    }

    public async Task<Result> Handle(RemoveUserFromTaskListCommand request, CancellationToken cancellationToken)
    {
        var taskList = await _unitOfWork.TaskListRepository.GetByIdAsync(
            request.TaskListId, 
            request.OwnerId, 
            cancellationToken);

        if (taskList is null)
        {
            return Result.Failure(
                DomainErrors.TaskList.NotFound(request.TaskListId));
        }

        var user = await _unitOfWork.UserRepository.GetByIdAsync(
            request.UserId, 
            cancellationToken);

        if (user is null)
        {
            return Result.Failure(
                DomainErrors.User.NotFound(request.UserId));
        }

        var result = taskList.RemoveUser(user);
        if (!result)
        {
            return Result.Failure(
                DomainErrors.User.NotAddedToTaskList(request.UserId, request.TaskListId));
        }

        await _unitOfWork.SaveChangesAsync();

        return Result.Success();
    }
}
