﻿using TaskListManager.Application.Abstractions.Messaging;

namespace TaskListManager.Application.TaskLists.Commands.RemoveUserFromTaskList;

public sealed record RemoveUserFromTaskListCommand(
    Guid TaskListId,
    Guid OwnerId,
    Guid UserId)
    : ICommand;