using FluentValidation;
using Gatherly.Persistence.Interceptors;
using MediatR;
using Microsoft.EntityFrameworkCore;
using Scrutor;
using TaskListManager.Application.Behaviors;
using TaskListManager.Domain.Repositories;
using TaskListManager.Persistance;
using TaskListManager.Persistance.Repositories;

var builder = WebApplication.CreateBuilder(args);

builder.Services.AddScoped<ITaskListRepository, TaskListRepository>();

builder
    .Services
    .Scan(
        selector => selector
            .FromAssemblies(
                TaskListManager.Persistance.AssemblyReference.Assembly)
            .AddClasses(false)
            .UsingRegistrationStrategy(RegistrationStrategy.Skip)
            .AsImplementedInterfaces()
            .WithScopedLifetime());

builder.Services.AddMemoryCache();

builder.Services.AddMediatR(TaskListManager.Application.AssemblyReference.Assembly);
builder.Services.AddScoped(typeof(IPipelineBehavior<,>), typeof(ValidationPipelineBehavior<,>));

builder.Services.AddValidatorsFromAssembly(
    TaskListManager.Application.AssemblyReference.Assembly,
    includeInternalTypes: true);

string connectionString = builder.Configuration.GetConnectionString("DefaultConnection");

builder.Services.AddSingleton<UpdateAuditableEntitiesInterceptor>();

builder.Services.AddDbContext<ApplicationDbContext>(
    (sp, optionsBuilder) =>
    {
        optionsBuilder.UseSqlServer(connectionString);
    });

builder
    .Services
    .AddControllers()
    .AddApplicationPart(TaskListManager.Persistance.AssemblyReference.Assembly);

builder.Services.AddSwaggerGen();

var app = builder.Build();

if (app.Environment.IsDevelopment())
{
    app.UseSwagger();
    app.UseSwaggerUI();
}

app.UseHttpsRedirection();

app.UseAuthorization();

app.MapControllers();

app.Run();
