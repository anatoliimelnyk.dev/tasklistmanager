﻿using Gatherly.Presentation.Contracts.Members;
using MediatR;
using Microsoft.AspNetCore.Mvc;
using TaskListManager.Application.User.Commands.CreateUser;
using TaskListManager.Application.User.Commands.UpdateUser;
using TaskListManager.Application.User.Queries;
using TaskListManager.Application.User.Queries.GetUserById;
using TaskListManager.Domain.Shared;
using TestListManager.Presentation.Abstractions;

namespace TestListManager.Presentation.Controllers
{
    [Route("api/users")]
    public sealed class UsersController : ApiController
    {
        public UsersController(ISender sender)
            : base(sender)
        {
        }

        [HttpGet("{id:guid}")]
        public async Task<IActionResult> GetUserById(Guid id, CancellationToken cancellationToken)
        {
            var query = new GetUserByIdQuery(id);

            Result<UserResponse> response = await Sender.Send(query, cancellationToken);

            return response.IsSuccess ? Ok(response.Value) : NotFound(response.Error);
        }

        [HttpPost]
        public async Task<IActionResult> RegisterUser(
            [FromBody] RegisterUserRequest request,
            CancellationToken cancellationToken)
        {
            var command = new CreateUserCommand(
                request.Email,
                request.FirstName,
                request.LastName);

            Result<Guid> result = await Sender.Send(command, cancellationToken);

            if (result.IsFailure)
            {
                return HandleFailure(result);
            }

            return CreatedAtAction(
                nameof(GetUserById),
                new { id = result.Value },
                result.Value);
        }

        [HttpPut("{id:guid}")]
        public async Task<IActionResult> UpdateUser(
            Guid id,
            [FromBody] UpdateUserRequest request,
            CancellationToken cancellationToken)
        {
            var command = new UpdateUserCommand(
                id,
                request.FirstName,
                request.LastName);

            Result result = await Sender.Send(
                command,
                cancellationToken);

            if (result.IsFailure)
            {
                return HandleFailure(result);
            }

            return NoContent();
        }
    }
}
