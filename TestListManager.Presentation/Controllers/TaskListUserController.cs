﻿using MediatR;
using Microsoft.AspNetCore.Mvc;
using TaskListManager.Application.TaskLists.Commands.AddUserToTaskList;
using TaskListManager.Application.TaskLists.Commands.RemoveUserFromTaskList;
using TaskListManager.Application.TaskLists.Queries.GerRelatedUsersByTaskId;
using TaskListManager.Application.User.Queries;
using TaskListManager.Domain.Shared;
using TestListManager.Presentation.Abstractions;
using TestListManager.Presentation.Contracts.TaskList;

namespace TestListManager.Presentation.Controllers;

[Route("api/tasklistuser")]
public class TaskListUserController : ApiController
{
    public TaskListUserController(ISender sender)
            : base(sender) { }

    [HttpGet("{id:guid}/{userId:Guid}")]
    public async Task<IActionResult> GetRelatedUsersByTaskListId(
    Guid id,
    Guid userId,
    CancellationToken cancellationToken)
    {
        var query = new GetRelatedUsersByTaskListIdQuery(id, userId);

        Result<IEnumerable<UserResponse>> response = await Sender.Send(
            query,
            cancellationToken);

        return response.IsSuccess
            ? Ok(response.Value)
            : NotFound(response.Error);
    }

    [HttpPut("{id:guid}")]
    public async Task<IActionResult> AddRelatedUserToTaskList(
            Guid id,
            [FromBody] AddRelatedUserToTaskListRequest request,
            CancellationToken cancellationToken)
    {
        var command = new AddUserToTaskListCommand(
            id,
            request.OwnerId,
            request.RelatedUserId);

        Result result = await Sender.Send(
            command,
            cancellationToken);

        if (result.IsFailure)
        {
            return HandleFailure(result);
        }

        return NoContent();
    }

    [HttpDelete("{id:guid}")]
    public async Task<IActionResult> RemoveRelatedUserFromTaskList(
        Guid id,
        [FromQuery] RemoveRelatedUserFromTaskListRequest request,
        CancellationToken cancellationToken)
    {
        var command = new RemoveUserFromTaskListCommand(
            id,
            request.OwnerId,
            request.RelatedUserId);

        Result result = await Sender.Send(
            command,
            cancellationToken);

        if (result.IsFailure)
        {
            return HandleFailure(result);
        }

        return NoContent();
    }
}
