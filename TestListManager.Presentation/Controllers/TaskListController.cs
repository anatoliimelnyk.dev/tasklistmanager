﻿using MediatR;
using Microsoft.AspNetCore.Mvc;
using TaskListManager.Application.TaskLists.Commands.AddUserToTaskList;
using TaskListManager.Application.TaskLists.Commands.CreateTaskList;
using TaskListManager.Application.TaskLists.Commands.RemoveUserFromTaskList;
using TaskListManager.Application.TaskLists.Commands.UpdateTaskList;
using TaskListManager.Application.TaskLists.Queries;
using TaskListManager.Application.TaskLists.Queries.GerRelatedUsersByTaskId;
using TaskListManager.Application.TaskLists.Queries.GetAllAsync;
using TaskListManager.Application.TaskLists.Queries.GetTaskListById;
using TaskListManager.Application.User.Queries;
using TaskListManager.Domain.Shared;
using TestListManager.Presentation.Abstractions;
using TestListManager.Presentation.Contracts.TaskList;

namespace TestListManager.Presentation.Controllers
{
    [Route("api/tasklist")]
    public sealed class TaskListController : ApiController
    {
        public TaskListController(ISender sender) 
            : base(sender)
        {
        }

        [HttpGet("{id:guid}/{userId:Guid}")]
        public async Task<IActionResult> GetTaskListById(
        Guid id,
        Guid userId,
        CancellationToken cancellationToken)
        {
            var query = new GetTaskListByIdQuery(id, userId);

            Result<TaskListResponse> response = await Sender.Send(
                query,
                cancellationToken);

            return response.IsSuccess
                ? Ok(response.Value)
                : NotFound(response.Error);
        }

        [HttpGet("{userId:guid}")]
        public async Task<IActionResult> GetAllTasks(
        Guid userId,
        [FromQuery]int skip,
        [FromQuery] int take,
        CancellationToken cancellationToken)
        {
            var query = new GetAllTaskListQuery(userId, skip, take);

            Result<IEnumerable<TaskListResponse>> response = await Sender.Send(
                query,
                cancellationToken);

            return response.IsSuccess
                ? Ok(response.Value)
                : NotFound(response.Error);
        }

        [HttpPost]
        public async Task<IActionResult> CreateTaskList(
            [FromBody] CreateTaskListRequest request,
            CancellationToken cancellationToken)
        {
            var command = new CreateTaskListCommand(
                request.UserId,
                request.Name);

            Result<Guid> result = await Sender.Send(command, cancellationToken);

            if (result.IsFailure)
            {
                return HandleFailure(result);
            }

            return CreatedAtAction(
                nameof(GetTaskListById),
                new { id = result.Value },
                result.Value);
        }

        [HttpPut("{id:guid}")]
        public async Task<IActionResult> UpdateTaskList(
            Guid id,
            [FromBody] UpdateTaskListRequest request,
            CancellationToken cancellationToken)
        {
            var command = new UpdateTaskListCommand(
                id,
                request.UserId,
                request.Name);

            Result result = await Sender.Send(
                command,
                cancellationToken);

            if (result.IsFailure)
            {
                return HandleFailure(result);
            }

            return NoContent();
        }
    }
}
