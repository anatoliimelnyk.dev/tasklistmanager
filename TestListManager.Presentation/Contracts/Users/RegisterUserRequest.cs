﻿namespace Gatherly.Presentation.Contracts.Members;

public sealed record RegisterUserRequest(
    string Email,
    string FirstName,
    string LastName);
