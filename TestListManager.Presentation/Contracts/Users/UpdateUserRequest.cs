﻿namespace Gatherly.Presentation.Contracts.Members;

public sealed record UpdateUserRequest(
    string FirstName, 
    string LastName);
