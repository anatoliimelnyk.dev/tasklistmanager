﻿namespace TestListManager.Presentation.Contracts.TaskList;

public sealed record RemoveRelatedUserFromTaskListRequest(
    Guid OwnerId,
    Guid RelatedUserId);
