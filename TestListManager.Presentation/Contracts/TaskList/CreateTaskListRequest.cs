﻿namespace TestListManager.Presentation.Contracts.TaskList;

public sealed record CreateTaskListRequest(
    Guid UserId,
    string Name);
