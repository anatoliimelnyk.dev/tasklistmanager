﻿namespace TestListManager.Presentation.Contracts.TaskList;

public sealed record UpdateTaskListRequest(
    Guid UserId,
    string Name);
