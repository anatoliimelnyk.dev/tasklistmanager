﻿namespace TestListManager.Presentation.Contracts.TaskList;

public sealed record AddRelatedUserToTaskListRequest(
    Guid OwnerId,
    Guid RelatedUserId);
