﻿using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace TaskListManager.Persistance.Migrations
{
    /// <inheritdoc />
    public partial class TaskListUserChangePK : Migration
    {
        /// <inheritdoc />
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_TaskListUser_TaskList_UserId",
                table: "TaskListUser");

            migrationBuilder.DropForeignKey(
                name: "FK_TaskListUser_User_TaskListId",
                table: "TaskListUser");

            migrationBuilder.AddForeignKey(
                name: "FK_TaskListUser_TaskList_TaskListId",
                table: "TaskListUser",
                column: "TaskListId",
                principalTable: "TaskList",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_TaskListUser_User_UserId",
                table: "TaskListUser",
                column: "UserId",
                principalTable: "User",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
        }

        /// <inheritdoc />
        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_TaskListUser_TaskList_TaskListId",
                table: "TaskListUser");

            migrationBuilder.DropForeignKey(
                name: "FK_TaskListUser_User_UserId",
                table: "TaskListUser");

            migrationBuilder.AddForeignKey(
                name: "FK_TaskListUser_TaskList_UserId",
                table: "TaskListUser",
                column: "UserId",
                principalTable: "TaskList",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_TaskListUser_User_TaskListId",
                table: "TaskListUser",
                column: "TaskListId",
                principalTable: "User",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
        }
    }
}
