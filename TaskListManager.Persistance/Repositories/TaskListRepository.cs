﻿using Microsoft.EntityFrameworkCore;
using TaskListManager.Domain.Entities;
using TaskListManager.Domain.Repositories;

namespace TaskListManager.Persistance.Repositories;

public class TaskListRepository : ITaskListRepository
{
    private readonly ApplicationDbContext _dbContext;

    public TaskListRepository(ApplicationDbContext dbContext)
    {
        _dbContext = dbContext;
    }

    public void Add(TaskList taskList)
    {
        _dbContext.Set<TaskList>().Add(taskList);
    }

    public async Task<IEnumerable<TaskList>> GetAllAsync(Guid userId, int skip, int take, CancellationToken cancellationToken = default)
    {
        return await _dbContext.Set<TaskList>()
            .Include(t => t.Owner)
            .Include(t => t.TaskListUsers)
            .ThenInclude(u => u.User)
            .Where(t => t.Owner.Id == userId || t.TaskListUsers.Any(u => u.UserId == userId))
            .OrderByDescending(t => t.CreatedOnUtc)
            .Skip(skip)
            .Take(take)
            .ToListAsync(cancellationToken);
    }

    public async Task<TaskList?> GetByIdAsync(Guid id, Guid userId, CancellationToken cancellationToken = default)
    {
        return await _dbContext.Set<TaskList>()
            .Include(t => t.Owner)
            .Include(t => t.TaskListUsers)
            .ThenInclude(u => u.User)
            .FirstOrDefaultAsync(t => t.Id == id 
            && (t.Owner.Id == userId || t.TaskListUsers.Any(u => u.UserId == userId)), 
            cancellationToken);
    }

    public async Task<TaskList?> GetByIdForRemoveAsync(Guid id, Guid userId, CancellationToken cancellationToken = default)
    {
        return await _dbContext.Set<TaskList>()
            .FirstOrDefaultAsync(t => t.Id == id && t.Owner.Id == userId, cancellationToken);
    }

    public async Task<IEnumerable<User>> GetRelatedUsersByTaskListIdAsync(Guid id, Guid userId, CancellationToken cancellationToken = default)
    {
        var taskList = await _dbContext.Set<TaskList>()
            .Include(t => t.Owner)
            .Include(t => t.TaskListUsers)
            .ThenInclude(u => u.User)
            .FirstOrDefaultAsync(t => t.Id == id
            && (t.Owner.Id == userId || t.TaskListUsers.Any(u => u.UserId == userId)),
            cancellationToken);

        if (taskList is null)
        {
            return Enumerable.Empty<User>();
        }

        return taskList.TaskListUsers.Select(c => c.User);
    }

    public void Remove(TaskList taskList)
    {
        _dbContext.Set<TaskList>().Remove(taskList);
    }

    public void Update(TaskList taskList)
    {
        _dbContext.Set<TaskList>().Update(taskList);
    }
}
