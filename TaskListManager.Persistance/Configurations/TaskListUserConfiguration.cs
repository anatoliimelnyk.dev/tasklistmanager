﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using TaskListManager.Domain.Entities;

namespace TaskListManager.Persistance.Configurations;

internal sealed class TaskListUserConfiguration : IEntityTypeConfiguration<TaskListUser>
{
    public void Configure(EntityTypeBuilder<TaskListUser> builder)
    {
        builder
            .HasKey(tlu => new { tlu.TaskListId, tlu.UserId });

        builder
            .HasOne(tlu => tlu.TaskList)
            .WithMany(u => u.TaskListUsers)
            .HasForeignKey(tlu => tlu.TaskListId);

        builder
            .HasOne(tlu => tlu.User)
            .WithMany(tl => tl.TaskListUsers)
            .HasForeignKey(tlu => tlu.UserId);
    }
}
