﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using TaskListManager.Domain.Entities;
using TaskListManager.Domain.ValueObjects;

namespace TaskListManager.Persistance.Configurations;

internal sealed class TaskListConfiguration : IEntityTypeConfiguration<TaskList>
{
    public void Configure(EntityTypeBuilder<TaskList> builder)
    {
        builder.ToTable(nameof(TaskList));

        builder.HasKey(x => x.Id);

        builder
            .Property(x => x.Name)
            .HasConversion(x => x.Value, v => TaskListName.Create(v).Value)
            .HasMaxLength(TaskListName.MaxLength);

        builder
            .HasOne(c => c.Owner)
            .WithMany();
    }
}

